import React, { useEffect, useState } from "react";
import Head from "next/head";
import Pusher from "pusher-js";
import styled from "styled-components";

import { transformRequest } from "@utils/index";
import { getContentfulEntries } from "@utils/contentful";

import {
  Card,
  Chip,
  CardContent,
  Container,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";

import { Alert } from "@material-ui/lab";
import { CheckCircle } from "@material-ui/icons";
import { green } from "@material-ui/core/colors";

const StyledPaper = styled(Paper)`
  margin: 1.5rem 0;
`;

const StyledCard = styled(Card)`
  margin-bottom: 1.5rem;

  &:last-of-type {
    margin-bottom: 0;
  }
`;

const pusher = new Pusher(process.env.PUSHER_KEY, {
  cluster: "eu",
});

Pusher.logToConsole = process.env.NODE_ENV !== "production";

const STATUS_CHANNEL = "statusEvent";
const GITLAB_EVENT = "gitlab";
const CLOUDFLARE_EVENT = "cloudflare";

const events = [GITLAB_EVENT, CLOUDFLARE_EVENT];

const createData = (name, uptime, status) => {
  return { name, uptime, status };
};

const entry = "7mGUVz7ra0asoVZjQ2UILv";

const Index = ({ contentfulIncidents = [] }) => {
  const [data, setData] = useState([]);
  const [incidents, setIncidents] = useState(contentfulIncidents);
  const [internal, setInternalData] = useState([
    createData("Hybris Node 1", 99, "Operational"),
    createData("GraphQL", 99, "Operational"),
    createData("Custom FE", 99, "Operational"),
    createData("Exegol FE", 100, "Operational"),
  ]);
  const [external, setEnternalData] = useState([
    createData("AWS EC2", 99, "Operational"),
    createData("AWS Lambda", 99, "Operational"),
    createData("AWS S3", 99, "Operational"),
    createData("Cloudflare", 100, "Operational"),
    createData("Cloudinary", 100, "Operational"),
    createData("Gitlab", 98, "Operational"),
  ]);

  const updateState = ({ service, ...body }) => {
    setData((prev) => {
      return {
        ...prev,
        [service]: body,
      };
    });
  };

  useEffect(() => {
    const subscribe = () => {
      const channel = pusher.subscribe(STATUS_CHANNEL);

      events.forEach((event) => channel.bind(event, updateState));
    };

    subscribe();

    return subscribe;
  }, []);

  return (
    <Container maxWidth="md">
      <Head>
        <title>Statuspage</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <CardContent>
          <Typography variant="h2" align="center" gutterBottom>
            Statuspage
          </Typography>
        </CardContent>

        <Alert severity="success">All Internal Systems Operational</Alert>

        <StyledPaper>
          <CardContent>
            <Typography variant="h6">Internal Services</Typography>
          </CardContent>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Source</TableCell>
                  <TableCell align="right">Status</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {internal.map((row) => (
                  <TableRow key={row.name}>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="right">
                      <CheckCircle style={{ color: green[500] }} />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </StyledPaper>

        <StyledPaper>
          <CardContent>
            <Typography variant="h6">RSS</Typography>
          </CardContent>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Source</TableCell>
                  <TableCell align="right">Status</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {Object.entries(data).map((item) => {
                  const [service, body] = item;
                  return (
                    <TableRow key={service}>
                      <TableCell component="th" scope="row">
                        {service}
                      </TableCell>
                      <TableCell align="left">{transformRequest({ service, body })}</TableCell>
                      <TableCell align="right">
                        <CheckCircle style={{ color: green[500] }} />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </StyledPaper>

        <StyledPaper>
          <CardContent>
            <Typography variant="h6">External Services</Typography>
          </CardContent>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Source</TableCell>
                  <TableCell align="right">Status</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {external.map((row) => (
                  <TableRow key={row.name}>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="right">
                      <CheckCircle style={{ color: green[500] }} />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </StyledPaper>

        <StyledPaper>
          <CardContent>
            <Typography variant="h6" gutterBottom>
              Past Incidents
            </Typography>
            <div>
              {incidents.map((incident, i) => (
                <StyledCard variant="outlined" key={i}>
                  <CardContent>
                    <Typography color="textSecondary" gutterBottom>
                      {incident.fields.title}
                    </Typography>
                    <Typography variant="body2" component="p">
                      {incident.fields.description.content[0].content[0].value}
                    </Typography>
                    <Typography variant="body2" component="p">
                      {incident.fields.status} - {incident.fields.date}
                    </Typography>
                  </CardContent>
                </StyledCard>
              ))}
            </div>
          </CardContent>
        </StyledPaper>
      </main>
    </Container>
  );
};

export const getServerSideProps = async () => {
  try {
    let contentful = await getContentfulEntries(entry);

    const {
      [entry]: { fields },
    } = contentful;

    const { incidents } = fields;

    return {
      props: {
        contentfulIncidents: incidents,
      },
    };
  } catch {
    return {};
  }
};

export default Index;
