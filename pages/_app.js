import { Fragment } from "react";
import { createGlobalStyle } from "styled-components";
import normalize from "@styles/normalize";

const Global = createGlobalStyle`
	${normalize};

	html,
	body,
	#__next {
		height: 100vh;
		width: 100vw;
  }

	body {
		-webkit-font-smoothing: antialiased;
		text-rendering: optimizeLegibility;
		background-color: #fafafa;
	}
`;


function App({ Component, pageProps }) {
    return (
        <Fragment>
            <Global />
            <Component {...pageProps} />
        </Fragment>
    )
}

export default App