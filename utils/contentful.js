const client = require("contentful");

export const ContentfulClient = client.createClient({
  space: process.env.CONTENTFUL_SPACE,
  accessToken: process.env.CONTENTFUL_CONTENT_DELIVERY,
  host: "cdn.contentful.com",
});

export const getContentfulEntries = async (modules) => {
  const entries = typeof modules === "string" ? [modules] : modules;
  const locale = "en-GB";

  let data = await Promise.all(
    // Include specifices how many levels deep the fields should resolve
    entries.map(
      async (e) => await ContentfulClient.getEntry(e, { locale, include: 2 })
    )
  );

  data = data.reduce((acc, curr) => ({ ...acc, [curr.sys.id]: curr }), {});

  return data;
};
