import { Fragment } from "react";
import { format } from "date-fns";

import { Typography } from "@material-ui/core";

const Body = ({ service, body }) => {
  switch (service) {
    case "gitlab":
      return (
        <Fragment>
          <Typography variant="h6">Time: {body.pubDate}</Typography>
          <Typography variant="h6">{body.description}</Typography>
          <a href={body.link} target="_blank">
            <Typography variant="h6">More Info</Typography>
          </a>
        </Fragment>
      );
    case "cloudflare":
      return (
        <Fragment>
          <Typography variant="h6">Time: {body.pubDate}</Typography>
          <Typography variant="h6">{body.content}</Typography>
          <a href={body.link} target="_blank">
            <Typography variant="h6">More Info</Typography>
          </a>
        </Fragment>
      );
    default:
      return null;
  }
};

export const transformRequest = ({ service, body }) => {
  return <Fragment>{Body({ service, body })}</Fragment>;
};
