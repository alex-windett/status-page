import styled from 'styled-components';

const Container = styled.div`
    padding-left: 4rem;
    padding-right: 4rem;
    max-width: 48rem;
    margin: 0 auto;
`;

export default Container;