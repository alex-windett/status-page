import styled from 'styled-components';

const StatusBar = styled.div`
    background-clip: border-box;
    background-color: #00866e;
    border-color: #edf2f9;
    border-radius: .5rem;
    border: 1px solid #edf2f9;
    box-shadow: 0 0.75rem 1.5rem rgba(18,38,63,.03);
    display: flex;
    flex-direction: column;
    margin-bottom: 1.5rem;
    min-width: 0;
    position: relative;
    word-wrap: break-word;
`;

export default StatusBar;